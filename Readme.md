# NLOG Demo

![NLogTest screenshot](NLogTest.png "Screenshot")


## Purpose

This shows a Windows Form with a TextBox and a "Add to Log" button.
Each time the Add to Log button is clicked, a log message is added to two NLog targets:

	1.  A log file placed in C:\windows\temp (assumes this directory exists and can be written to by logged in user)
	2.  A read-only textbox in the form.

The log message contains the current date/time.


## Enhancement

David Ching wrote BetterFormControlTarget that appends text more efficiently by using TextBox.AppendText.
However, this requires the Target is given a WinForms TextBox control, not any control with a .Text property.

The code uses

	#define USE_DAVIDS_BETTER_NLOG_FORM_CONTROL

to enable David's improved target.  

To use the original target instead, comment out this line.


## Bonus

The Windows Form resizes and the Log textbox grows and shrinks along with it.  This demonstrates use of a TableLayoutPanel and
setting Anchor and Dock properties of each control.

### Limitation

I only got this to work by placing the resizable Log textbox at the bottom.  I did not get the Log textbox to
resize successfully when I tried to move the fixed-sized button below it.
