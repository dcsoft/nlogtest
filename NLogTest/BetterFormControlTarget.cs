﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using NLog.Common;
using NLog.Targets;
using NLog;


/// <summary>
/// Based on NLog.Windows.Forms-master\NLog.Windows.Forms, version 4.2.3
/// Source:  https://github.com/NLog/NLog.Windows.Forms
/// 
/// This has improved performance by using TextBox.AppendText, but it requires a WinForms TextBox
/// be the target, not any control with a .Text property
/// </summary>


namespace NLogTest
{
    /// <summary>
    /// Logs text to Windows.Forms.Control.Text property control of specified Name.
    /// </summary>
    /// <example>
    /// <p>
    /// To set up the target in the <a href="config.html">configuration file</a>, 
    /// use the following syntax:
    /// </p>
    /// <code lang="XML" source="examples/targets/Configuration File/FormControl/NLog.config" />
    /// <p>
    /// The result is:
    /// </p>
    /// <img src="examples/targets/Screenshots/FormControl/FormControl.gif" />
    /// <p>
    /// To set up the log target programmatically similar to above use code like this:
    /// </p>
    /// <code lang="C#" source="examples/targets/Configuration API/FormControl/Form1.cs" />,
    /// </example>
    [Target("FormControl")]
    public sealed class BetterFormControlTarget : TargetWithLayout
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormControlTarget" /> class.
        /// </summary>
        /// <remarks>
        /// The default value of the layout is: <code>${longdate}|${level:uppercase=true}|${logger}|${message}</code>
        /// </remarks>
        public BetterFormControlTarget()
        {
            Append = true;
        }

        public TextBox TextBox { private get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether log text should be appended to the text of the control instead of overwriting it. </summary>
        /// <docgen category='Form Options' order='10' />
        [DefaultValue(true)]
        public bool Append { get; set; }

        /// <summary>
        /// Gets or sets whether new log entry are added to the start or the end of the control
        /// </summary>
        public bool ReverseOrder { get; set; }

        /// <summary>
        /// Log message to control.
        /// </summary>
        /// <param name="logEvent">
        /// The logging event.
        /// </param>
        protected override void Write(LogEventInfo logEvent)
        {
            // TextBox.set() can be called on another thread while this method is running
            // Use the same control throughout the execution of this function.
            TextBox tb = TextBox;

            if (tb == null)
                return;

            string logMessage = Layout.Render(logEvent);

            try
            {
                tb.BeginInvoke( (MethodInvoker) delegate()
                {
                    //append of replace?
                    if (Append)
                    {
                        //beginning or end?
                        if (ReverseOrder)
                            tb.Text = logMessage + tb.Text;
                        else
                            tb.AppendText(logMessage); // much faster than TextBox.Text += logMessage;
                    }
                    else
                    {
                        tb.Text = logMessage;
                    }
                });
            }
            catch (Exception ex)
            {
                InternalLogger.Warn(ex.ToString());

                if (LogManager.ThrowExceptions)
                {
                    throw;
                }
            }
        }
    }
}
