﻿//#define USE_DAVIDS_BETTER_NLOG_FORM_CONTROL

using System.Windows.Forms;
using NLog;
using NLog.Config;
using NLog.Targets;
using NLog.Windows.Forms;

namespace NLogTest
{
    public partial class Form1 : Form
    {
        private const string LogFilePath = @"c:\windows\temp\mylog.txt";
        private Logger logger = null;

        public Form1()
        {
            InitializeComponent();

            InitLog();
        }

        private void InitLog()
        {
            // Log to file
            var minLevel = LogLevel.Trace;
            var config = new LoggingConfiguration();
            var fileLayout = "${date:format=MM/dd/yyyy HH\\:mm\\:ss} | ${level} | ${message}";

            FileTarget fileTarget = new FileTarget
            {
                Layout = fileLayout,
                FileName = LogFilePath,
                ConcurrentWrites = true,                      // this speeds up things when no other processes are writing to the file
            };
            config.AddTarget("FileTarget", fileTarget);
            config.LoggingRules.Add(new LoggingRule("*", minLevel, fileTarget));


            var textBoxLayout = fileLayout + " | ${newline}";
#if USE_DAVIDS_BETTER_NLOG_FORM_CONTROL
            var formControlTarget = new BetterFormControlTarget
            {
                TextBox = textBoxLog,
#else
            var formControlTarget = new FormControlTarget
            {
                FormName = "Form1",
                ControlName = "textBoxLog",
#endif
                Layout = textBoxLayout,
                Append = true,
                ReverseOrder = false
            };
            config.AddTarget("FormControlTarget", formControlTarget);
            config.LoggingRules.Add(new LoggingRule("*", minLevel, formControlTarget));

            LogManager.Configuration = config;
            logger = LogManager.GetLogger("Form1");
        }

        private void buttonAddToLog_Click(object sender, System.EventArgs e)
        {
            logger.Trace("It's cool");
        }
    }
}
